---
title: "Titulos del podcast"
description: |
    Descripción corta
podcast_image: "logo_podcast.png" # Fichero con el logo

author:
  name: "David Marzal" # <itunes:author|itunes:name>, <item>-<itunes:author>
  email: "kde_express@kde-espana.org" # <managingEditor|webMaster|itunes:email> <item>-<author>

URLprefix : https://

# https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md
p20:
# Datos que deben estar aqui definidos si se quieren usar
    guid: "ID"        # channel
    protocol: "activitypub"     # https://github.com/Podcastindex-org/podcast-namespace/blob/main/socialprotocols.txt
    accountId: "@kde_espana"    # social-interact       # item
    accountUrl: "https://floss.social/@kde_espana"      # item
# Datos especificos que pueden sustituir a los globales de hugo.toml
    locked: "no"                # ¿Feed bloqueado para otras plataformas? yes|no
    funding: "Hazte socio/a"    # Texto para animar a colaborar
    fundingURL: "https://www.kde-espana.org/involucrate" # Enlace
    location : "España"         # channel
    locationGEO : "40.416944,-3.703333;u=1000000"        # 
    locationOSM : "R1311341"    # España
    medium :                    # podcast (default) https://podcastindex.org/namespace/1.0#medium
    transcript :
        rel : captions
        language : es
    participantes :
      - person : David Marzal
        role : host
        group : cast
        img : "https://mastodon.escepticos.es/system/accounts/avatars/110/741/007/953/649/588/original/4422d774516e1cc5.jpg"
        href : https://masto.es/@DavidMarzalC
      - person : 
        role : co-host
        group : cast
        img : 
        href : 
    podroll : [e7fc1a59-fb05-5202-a94c-4e18750cddba, 68b52970-05d8-5cb1-8bd3-d15fa3961e09, bd8f7280-b35a-5bfd-bbde-829cf1b12682, 1f792690-bccb-534e-b95d-52dc00426822, 2a4a6311-ddad-5735-9ed7-09d734d8d041, 610e9ea8-edf0-407f-9e6c-72375a0e17db, 093ce4f4-ee17-5746-b334-a423d54dd018]
    #podping : [5970771, 6571346]
    websub : "https://hub.livewire.io/"
    publisher :
      feedGuid :
      feedUrl : 
    # chat:
    #     server : kde.org
    #     protocol : matrix
    #     accountId : "@dmarzal:matrix.org"
    #     space : "#kde-es:kde.org"
    # follow : followlinks
skipHours: [0, 1, 2, 3, 4, 5, 20, 21, 22, 23]
#skipDays: [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
itunes:
    explicit:                   # por defecto false

# A continuación poner la descripción del podcast con html
---
La descripción de tu podcast ampliada
