---
title: "#00 Titulo episodio"
date: 2023-11-26
categories: []
author: Tu mismo
tags: [podcast, temporada1]
img: 
podcast:
  audio:https://op3.dev/archive.org/download/sin extension
  olength :
  mlength :
  iduration : "00:01:03"
  siuri : "URL a mensaje de Mastodon"
  trurl : URL a fichero con subtitutlos.vtt
participantes :
  - person : David Marzal
    role : host
    group : cast
    img : "https://mastodon.escepticos.es/system/accounts/avatars/110/741/007/953/649/588/original/4422d774516e1cc5.jpg"
    href : https://masto.es/@DavidMarzalC
  - person : 
    role : co-host
    group : cast
    img : 
    href : 
p20:
  #trtype : "text/vtt" # text/plain text/html text/vtt application/json application/x-subrip
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /número_episodio
---
Notas del episodio

Este podcast tiene licencia [Reconocimiento-CompartirIgual 4.0 Internacional (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).
