# Hugo Cards Podcasts

Tema y ejemplo de repositorio para usar un fork "mejorado" de https://github.com/bul-ikana/hugo-cards para crear una web y feed para tus podcast.

## Casos prácticos

- Teneís en https://gitlab.com/kdeexpress/kdeexpress.gitlab.io un ejemplo actualizado de como se está usando este tema/proyecto.
- Y en https://gitlab.com/podcastlinux/podcastlinux.gitlab.io un ejemplo con 2 podcast diferentes bajo la misma web.

## Uso

Cada carpeta (categoria) bajo `content/posts/` es para un podcast diferente que '`layouts/section/rss.xml` se encargará de recorrer sus articulos y generar el feed correspondiente.

El proyecto en si está pensado para clonar en un "grupo" de Gitlab y asi publicarlo en Gitlab pages, pero el único requisito real es `hugo` ya que se puede generar el codigo web completo (carpeta public) y subirlo a cualquier sitio (pero esto puede requerir alguna modificación).

### Gitlab
- Tener un Grupo de Gitlab con el nombre que querais que tenga la URL principal del pryecto https://gitlab.com/$GRUPO
- Clonar este repo y subirlo dentro de ese Grupo y cambiarle el nombre
- Cambiar la ruta de acceso a vuestro proyecto en `Setting -> General -> Advanced -> Change path`

### Podcasts

Ficheros a modificar:
- Datos Globales de la web y todos los podcast: [/hugo.toml](hugo.toml)
- Datos de cada podcast: [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
- Ficheros de episodios: [/content/posts/$tupodcast/##](content/posts/tupodcast/00-Podcast-.md)
- Añadir logo del podcast en : [content/posts/$tupodcast/](/content/posts/$tupodcast/)logo_podcast.png (nombre configurable)

## Podcasting 2.0

### podcast:guid
- Configurable en `<channel>`: `guid` [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
- Usado en: [Buscar](https://gitlab.com/search?search=podcast%3Aguid&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#guid

### podcast:funding
- Configurable en `<channel>`: `funding` + `fundingURL`
    - Global para todos [/hugo.toml ](hugo.toml) o para cada podcast [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
- Usado en: [Buscar](https://gitlab.com/search?search=podcast%3Afunding&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#funding

### podcast:locked
- Configurable en `<channel>`: 
    - `locked` [/hugo.toml](hugo.toml) o [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
    - `Author.email` (opcional) [/hugo.toml](hugo.toml)
- Usado en: [Buscar](https://gitlab.com/search?search=podcast%3Alocked&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#locked

### podcast:medium
- Configurable en `<channel>`:
    - Global en [/hugo.toml](hugo.toml) o por podcast [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
- Usado en: [Buscar](https://gitlab.com/search?search=podcast%3Amedium&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#medium

### podcast:license 
- Configurable en `<channel>`: `license` + `licenseURL` (opcional) [/hugo.toml ](hugo.toml)
- Usado en: [Buscar](https://gitlab.com/search?search=podcast%3Alicense&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#license

### podcast:location
- Configurable en: `location` + `locationGEO` (recomendado) + `locationOSM` (recomendado)
    - `<channel>` : Global en [/hugo.toml](hugo.toml) o por podcast [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
    - `<item>` : [content/posts/tupodcast/##-episodio.md]([content/posts/tupodcast/00-Podcast-.md]) 
- Usado en: `<channel>` y/o en `<item>` - [Buscar](https://gitlab.com/search?search=podcast%3Alocation&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#location

### podcast:person
- Configurable en: `person` + `role` (opt) + `group` (opt) + `img` (opt) + `href` (opt)
    - `<channel>` : Por podcast [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
    - `<item>` : [content/posts/tupodcast/##-episodio.md]([content/posts/tupodcast/00-Podcast-.md]) 
- Usado en: `<channel>` y en `<item>` - [Buscar](https://gitlab.com/search?search=podcast%3Aperson&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#person

### podcast:socialInteract
- Configurable en `<item>`:
    - `uri` [content/posts/tupodcast/##-episodio.md]([content/posts/tupodcast/00-Podcast-.md])
    - `protocol` [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md)
    - `accountId` [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md) (recomendado)
    - `accountUrl` [/content/posts/$tupodcast/_index.md](content/posts/tupodcast/_index.md) (opcional)
- Usado en: [Buscar](https://gitlab.com/search?search=podcast%3AsocialInteract&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#social-interact

### podcast:transcript
- Configurable en `<item>`: 
    - `trurl` [content/posts/tupodcast/##-episodio.md]([content/posts/tupodcast/00-Podcast-.md])
    - `trtype` [content/posts/tupodcast/##-episodio.md]([content/posts/tupodcast/00-Podcast-.md]) o Global en [/hugo.toml](hugo.toml)
- Usado en: [Buscar](https://gitlab.com/search?search=podcast%3Atranscript&nav_source=navbar&project_id=19126273&search_code=true&repository_ref=master)
- Especficación: https://podcastindex.org/namespace/1.0#transcript


## Sin implementar aún
### podcast:updateFrequency
- Especficación: https://podcastindex.org/namespace/1.0#update-frequency
- Utilidad: https://update-frequency.vercel.app/
    - [Fuente (github)](https://github.com/nathangathright/updateFrequency)


## Licencia
Todo el contenido propio del proyecto se libera bajo Licencia [Creative Commons Atribución - Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/)  
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>


[TOC]
